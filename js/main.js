'use strict';

/***********************************************************************************/
/* *********************************** DONNEES *************************************/
/***********************************************************************************/

//déclarer les éléments HTML en variable dans JS pour que les fonctions puissent intéragissent avec le HTML
let button = document.getElementById('firing-button');
let span = document.querySelector("#billboard span");
let count = 4;
let timer;
let rocket = document.querySelector("#rocket")
let main = document.querySelector('main')
let audio=document.querySelector('audio')


/***********************************************************************************/
/* ********************************** FONCTIONS ************************************/
/***********************************************************************************/

function decreaseCount() { //fonction pour me décompte et arréter le décompte
    count --
    span.textContent = count;
    if(count <= 0) { //condition
        console.log('stop')
        window.clearInterval(timer); //affichage temps et arrêt a zero
        rocket.src = "images/rocket3.gif"; //changer image de base par nouvelle
        rocket.classList.add('tookOff'); //rajouter image class tookoof
    }
}


function startCount(){ //fonction déclanche compte a rebours
    button.classList.add('disabled');//rajouter une classe au bouton
    button.removeEventListener('click', startCount);//bouton démarrage
    timer = window.setInterval(decreaseCount, 1000); //affichage le compte a rebours 
    rocket.src = "images/rocket2.gif";
    audio.play(); //jouer l'audio
}



function getRandom(min, max) { //fonction aleatoire pour créer des étoiles aléatoire sur le HTML
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function createStar() { //fonction pour créer DIV ou on aura les étoiles 
    let div = document.createElement('div'); //permet de créer l'élément
    div.classList.add('star');
    let size = getRandom(1, 3);
    
    switch(size) { //créer une boucle switch pour créer les class des div en fonction de la taille de l'étoile
        case 1:
            div.classList.add('tiny');
        break;
        
        case 2:
            div.classList.add('normal');
        break;
        
        case 3:
            div.classList.add('big');
        break;
    }
    
    //créer lES variable pour la position alatoire de l'étoile
    
    let leftRandomPosition = getRandom(0, 100); //etre 0 et 100 sur la page 
    let topRandomPosition = getRandom(0, 100); //entre 0 et 100
    
    div.style.left = leftRandomPosition+"%"; //mettre un css pour la position (concaténé avec le %)
    div.style.top = topRandomPosition+"%"; //mettre un css pour la position (concaténé avec le %)
    
    main.appendChild(div); //permet d'injecter les éroile en dernier 
}

/************************************************************************************/
/* ******************************** CODE PRINCIPAL **********************************/
/************************************************************************************/
for(let i = 0; i < 1000; i++) {  //boucle 1000 fois pour créer les étoiles 
    createStar()
}


button.addEventListener('click', startCount)